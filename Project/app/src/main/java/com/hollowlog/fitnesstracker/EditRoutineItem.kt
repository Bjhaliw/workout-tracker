package com.hollowlog.fitnesstracker

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class EditRoutineItem : AppCompatActivity() {
    private lateinit var exerciseTextView: TextView
    private lateinit var mDatabase: DatabaseReference
    private lateinit var mAuth: FirebaseAuth
    private lateinit var removeExerciseButton: Button
    private lateinit var keepExerciseButton: Button
    private lateinit var exercise: String

    companion object {
        private const val TAG = "EditRoutine"
        private const val REMOVE_EXCERCISE = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_routine_item)

        // Get data from the intent
        exercise = intent.getStringExtra("name").toString()

        // Get a reference to the Database
        mDatabase = FirebaseDatabase.getInstance().reference
        mAuth = FirebaseAuth.getInstance()

        // Initialize the text view and set it to the exercise name
        exerciseTextView = findViewById(R.id.routine_exercise_name_text)
        exerciseTextView.text = exercise

        // Initialize the remove exercise and keep exercise buttons
        removeExerciseButton = findViewById(R.id.remove_exercise)
        keepExerciseButton = findViewById(R.id.keep_exercise)

        // Create the listeners for each button
        removeExerciseButton.setOnClickListener { removeExercise() }
        keepExerciseButton.setOnClickListener { keepExercise() }
    }

    // Notify to keep the exercise
    private fun keepExercise() {
        val data = Intent()
        data.putExtra("request", "keep")
        setResult(RESULT_OK, data)
        finish()
    }

    // Notify to delete the exercise
    private fun removeExercise() {
        val data = Intent()
        data.putExtra("request", "delete")
        data.putExtra("exercise", exercise)
        setResult(RESULT_OK, data)
        finish()
    }
}