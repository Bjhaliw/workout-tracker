package com.hollowlog.fitnesstracker

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.util.*


class EditRoutines : AppCompatActivity() {

    companion object {
        private const val TAG = "RoutineList"
        private const val ADD_EXERCISE = 1
        private const val ADD_ROUTINE = 2
        private const val REMOVE_EXERCISE = 3
    }

    // Global variables for the RoutineList activity
    private lateinit var dropdownMenu: Spinner
    private lateinit var exerciseRecyclerView: RecyclerView
    private lateinit var mDatabase: DatabaseReference
    private lateinit var mAuth: FirebaseAuth
    private lateinit var listOfRoutines: List<String>
    private lateinit var currentExerciseList: List<ExerciseObject>
    private lateinit var newRoutineButton: Button
    private lateinit var addExerciseRoutineButton: Button
    private lateinit var deleteRoutineButton: Button
    private lateinit var routine: Any


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_routine)

        // Get a reference to the Database
        mDatabase = FirebaseDatabase.getInstance().reference
        mAuth = FirebaseAuth.getInstance()

        // Initialize the spinner
        dropdownMenu = findViewById(R.id.routine_dropdown_menu)

        currentExerciseList = listOf()

        // Load the dropdown to allow users to select their routine
        loadDropdownMenu()

        // Initialize the recycler view
        exerciseRecyclerView = findViewById(R.id.routine_current_exercise_recycler_view)
        exerciseRecyclerView.layoutManager = LinearLayoutManager(this)

        // Initialize the new routine, add exercise and delete exercise buttons
        newRoutineButton = findViewById(R.id.new_routine)
        addExerciseRoutineButton = findViewById(R.id.add_exercise_routine)
        deleteRoutineButton = findViewById(R.id.delete_routine)

        // Create the listeners for each button
        newRoutineButton.setOnClickListener { newRoutine() }
        addExerciseRoutineButton.setOnClickListener { addExerciseRoutine() }
        deleteRoutineButton.setOnClickListener { deleteRoutine() }
    }

    // Starts an activity to add an exercise to a routine
    private fun addExerciseRoutine() {
        val intent = Intent(this@EditRoutines, MuscleGroupExerciseActivity::class.java)
        intent.putExtra("request", "add")

        startActivityForResult(intent, ADD_EXERCISE)
    }

    // Deletes the selected routine
    private fun deleteRoutine() {
        // Prevents deleting any of the default routines
        if (routine.toString() == "Leg Day" || routine.toString() == "Push Day" || routine.toString() == "Pull Day") {
            val text = "Can't delete " + routine.toString() + " because it is a default routine."
            val toast = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT)
            toast.show()
        } else {
            mDatabase.child("User").child(mAuth.currentUser!!.uid).child("Routines")
                .child(routine.toString()).removeValue().addOnSuccessListener {
                    val text = "Deleted routine: " + routine.toString() + "."
                    val toast = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT)
                    toast.show()

                    loadDropdownMenu()
                }
        }
    }

    // Starts an activity to create a new routine
    private fun newRoutine() {
        val intent = Intent(this@EditRoutines, NewRoutine::class.java)
        intent.putExtra("request", "add")

        startActivityForResult(intent, ADD_ROUTINE)
    }

    // Load dropdown menu with the routines and populate the recycler view with exercises based on the selected routine
    private fun loadDropdownMenu() {
        listOfRoutines = listOf()
        mDatabase.child("User").child(mAuth.currentUser!!.uid).child("Routines").get()
            .addOnSuccessListener {

                it.children.forEach { value ->
                    listOfRoutines = listOfRoutines.plus(value.key.toString())
                }

                // Create an ArrayAdapter for the spinner to show items
                var aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, listOfRoutines)

                // Loading the dropdown menu portion of the spinner
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                // Assigning the spinner's adapter to the adapter created above
                dropdownMenu.adapter = aa
            }.addOnFailureListener {
                Log.e("firebase", "Error getting data", it)
            }

        // Create the listener for when the user selects an item in the spinner
        dropdownMenu.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Do nothing
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                // Update the selected routine
                routine = parent?.getItemAtPosition(position)!!
                // Populate recycler view with the selected routine's exercises
                mDatabase.child("User").child(mAuth.currentUser!!.uid).child("Routines")
                    .child(routine.toString()).get().addOnSuccessListener {
                        var tempArray = it.value as List<Object>
                        // Update current exercises for the selected routine
                        currentExerciseList = listOf<ExerciseObject>()

                        tempArray.forEach {
                            var tempHash = it as HashMap<String, Object>

                            var name = tempHash["exerciseName"] as String
                            var type = tempHash["exerciseType"] as String

                            currentExerciseList = currentExerciseList.plus(
                                ExerciseObject(
                                    name,
                                    type,
                                    listOf<SetObject>()
                                )
                            )
                        }

                        exerciseRecyclerView.adapter =
                            RoutineRecyclerViewAdapter(
                                currentExerciseList,
                                R.layout.list_item,
                                this@EditRoutines
                            )
                    }
            }
        }
    }

    // Update the recycler to reflect the current exercise list
    private fun updateRecyler() {
        exerciseRecyclerView.adapter =
            RoutineRecyclerViewAdapter(
                currentExerciseList,
                R.layout.list_item,
                this@EditRoutines
            )
    }

    // Update the dropdown to reflect the current list of routines
    private fun updateDropdown() {
        Collections.sort(listOfRoutines)
        // Create an ArrayAdapter for the spinner to show items
        var aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, listOfRoutines)
        // Loading the dropdown menu portion of the spinner
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Assigning the spinner's adapter to the adapter created above
        dropdownMenu.adapter = aa
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ADD_EXERCISE) {
            if (resultCode == RESULT_OK) {
                // Get data from the intent
                val exercise = data?.getStringExtra("name")!!
                val muscle = data?.getStringExtra("muscleGroup")!!

                // Add exercise to the routine in the database and the current exercise list
                mDatabase.child("User").child(mAuth.currentUser!!.uid).child("Routines")
                    .child(routine.toString())
                    .get().addOnSuccessListener {
                        var temp = it.value as List<ExerciseObject>
                        // Remove the placeholder exercise if necessary
                        if (currentExerciseList.get(0).exerciseName == "None" && currentExerciseList.size == 1) {
                            currentExerciseList =
                                listOf(ExerciseObject(exercise!!, muscle!!, listOf<SetObject>()))
                            temp = listOf(ExerciseObject(exercise!!, muscle!!, listOf<SetObject>()))
                        } else {
                            currentExerciseList = currentExerciseList.plus(
                                ExerciseObject(
                                    exercise!!,
                                    muscle!!,
                                    listOf<SetObject>()
                                )
                            )
                            temp =
                                temp.plus(ExerciseObject(exercise!!, muscle!!, listOf<SetObject>()))
                        }
                        updateRecyler()
                        mDatabase.child("User").child(mAuth.currentUser!!.uid).child("Routines")
                            .child(routine.toString())
                            .setValue(temp)
                    }
                val duration = Toast.LENGTH_SHORT
                val text = "$exercise has been added to $routine"
                val toast = Toast.makeText(applicationContext, text, duration)
                toast.show()
            }
        } else if (requestCode == ADD_ROUTINE) {
            if (resultCode == RESULT_OK) {
                // Get data from the intent
                val name = data?.getStringExtra("newName")!!
                // Get the list of current routines
                mDatabase.child("User").child(mAuth.currentUser!!.uid).child("Routines").get()
                    .addOnSuccessListener {
                        // Add routine with a placeholder exercise
                        var temp = listOf(ExerciseObject("None", "None", listOf<SetObject>()))
                        listOfRoutines = listOfRoutines.plus(name)
                        // Update the routine list in the database
                        mDatabase.child("User").child(mAuth.currentUser!!.uid).child("Routines")
                            .child(name).setValue(temp)
                        updateDropdown()
                    }
                val duration = Toast.LENGTH_SHORT
                val text = "$name has been added to routines"
                val toast = Toast.makeText(applicationContext, text, duration)
                toast.show()

            }
        } else if (requestCode == REMOVE_EXERCISE) {
            if (resultCode == RESULT_OK) {
                val request = data?.getStringExtra("request")
                if (request == "delete") {
                    // Get data from intent
                    val exercise = data?.getStringExtra("exercise")!!
                    // Get the exercises in the selected routine
                    mDatabase.child("User").child(mAuth.currentUser!!.uid).child("Routines")
                        .child(routine.toString())
                        .get().addOnSuccessListener {
                            var temp = it.value as List<ExerciseObject>
                            // Insert a placeholder if there are no exercises left in the routine
                            if (currentExerciseList.size == 1) {
                                currentExerciseList =
                                    listOf(ExerciseObject("None", "None", listOf<SetObject>()))
                                temp = listOf(ExerciseObject("None", "None", listOf<SetObject>()))
                            } else {
                                // Check the adapter's current position to determine which exercise to remove
                                val adapter =
                                    exerciseRecyclerView.adapter as RoutineRecyclerViewAdapter
                                val tempExerciseList = currentExerciseList.toMutableList()
                                tempExerciseList.removeAt(adapter.getCurrPosition())
                                currentExerciseList = tempExerciseList.toList()

                                val tempList = temp.toMutableList()
                                tempList.removeAt(adapter.getCurrPosition())
                                temp = tempList.toList()
                            }
                            updateRecyler()
                            // Update the routine's exercises in the database
                            mDatabase.child("User").child(mAuth.currentUser!!.uid).child("Routines")
                                .child(routine.toString())
                                .setValue(temp)
                        }
                    val duration = Toast.LENGTH_SHORT
                    val text = "Removed $exercise from routine"
                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.show()
                }
            }
        }
    }
}
