package com.hollowlog.fitnesstracker

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth

class LoginScreen : AppCompatActivity() {

    companion object {
        private const val TAG = "LoginScreen"
        private const val ADD_USER = 0
    }

    // Global variables
    private lateinit var welcomeText: TextView
    private lateinit var emailField: EditText
    private lateinit var passwordField: EditText
    private lateinit var loginButton: Button
    private lateinit var registerButton: Button

    // Firebase authentication ref
    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        welcomeText = findViewById(R.id.welcome_text)
        emailField = findViewById(R.id.login_email)
        passwordField = findViewById(R.id.login_password)
        loginButton = findViewById(R.id.login_button)
        registerButton = findViewById(R.id.register_button)

        mAuth = FirebaseAuth.getInstance()

        // Did we get here from another activity?
        val loginRequired = intent.getStringExtra("login")
        if (loginRequired != null) {
            Toast.makeText(this, "You must login to use this application", Toast.LENGTH_LONG).show()
        }

        // If there is a user signed in, show the logout button instead and their email
        if (mAuth.currentUser != null) {
            loginButton.text = "Log Out"
            welcomeText.visibility = View.VISIBLE
            welcomeText.text = "Signed in as ${mAuth.currentUser!!.email}"
            emailField.visibility = View.GONE
            passwordField.visibility = View.GONE
            registerButton.visibility = View.GONE
        }

        loginButton.setOnClickListener { login() }
        registerButton.setOnClickListener { registerUser() }
    }

    /**
     * The user pressed the login button on the screen, sign them into their account if valid
     */
    private fun login() {

        // If the user is logged in, then sign them out
        if (mAuth.currentUser != null) {
            logout()
            return
        }

        // Verify that the fields are valid
        val email = emailField.text.toString().trim()
        val password = passwordField.text.toString().trim()

        if (email.isEmpty()) {
            emailField.error = "Please enter your email"
            emailField.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailField.error = "Please enter a valid email"
            emailField.requestFocus()
            return
        }

        if (password.isEmpty()) {
            passwordField.error = "Please enter your password"
            passwordField.requestFocus()
            return
        }

        if (password.length < 6) {
            passwordField.error = "Minimum length is 6 characters"
            passwordField.requestFocus()
            return
        }

        // Sign into the Firebase Authentication with email and password
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (it.isSuccessful) {
                Toast.makeText(this, "Successfully logged in", Toast.LENGTH_LONG).show()
                setResult(RESULT_OK)
                finish()
            } else {
                Toast.makeText(this, "Email or password is incorrect", Toast.LENGTH_LONG).show()
            }
        }
    }

    /**
     * Log the user out of the application
     */
    private fun logout() {
        mAuth.signOut()
        loginButton.text = "Login"
        welcomeText.visibility = View.GONE
        emailField.visibility = View.VISIBLE
        passwordField.visibility = View.VISIBLE
        registerButton.visibility = View.VISIBLE
        Toast.makeText(this, "User has been signed out", Toast.LENGTH_LONG).show()
    }

    /**
     * Start an activity for the user to register a new account
     */
    private fun registerUser() {
        val intent = Intent(this@LoginScreen, RegisterUser::class.java)
        startActivityForResult(intent, ADD_USER)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_USER) {
                Toast.makeText(this, "Successfully logged in", Toast.LENGTH_LONG).show()
                setResult(RESULT_OK)
                finish()
            }
        }
    }

}